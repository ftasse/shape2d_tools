#include <float.h>
#include <stdlib.h>
#include <fstream>
#include <stack>

#include <pcl/common/common.h>
#include <pcl/common/time.h>
#include <pcl/console/print.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/io/png_io.h>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <vtkImageCanvasSource2D.h>
#include <vtkImageBlend.h>
#include <vtkImageCast.h>
#include <vtkPNGWriter.h>

// #include "opencv2/imgproc/imgproc.hpp"

extern "C" {
#include <vlfeat/vl/generic.h>
#include <vlfeat/vl/imopv.h>
#include <vlfeat/vl/sift.h>
}

#include "3d_tools/utils/cmd_parser.h"
#include "3d_tools/utils/matrix_io.h"

struct ContourNodeProperty
{
  int id;
  Eigen::Vector2i point;
};

struct ContourEdgeProperty
{
  std::vector<Eigen::Vector2i> points;
};

typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS, ContourNodeProperty, ContourEdgeProperty> ContourGraph;

typedef struct VKeypoint {
  std::pair<float, float> point;
  float size;
  float angle;
  int octave;
  float response;

  VKeypoint(std::pair<float, float> _point, float _size, float _angle, int _octave, float _response)
    : point(_point), size(_size), angle(_angle), octave(_octave), response(_response) {

  }
} VKeypoint;

void generate_sift_keypoints(float* image, int width, int height, int num_levels, int o_min, 
  std::vector<VKeypoint>& keypoints, std::vector<Eigen::RowVectorXf>& descriptors) {
  int num_octaves = log2(std::min(width,height));

  keypoints.clear();
  descriptors.clear();

  VlSiftFilt* sift_filter =  vl_sift_new (width, height, num_octaves, num_levels, o_min);
  if (vl_sift_process_first_octave (sift_filter, image)!= VL_ERR_EOF) {
    while (true) { // calculate the key points in each group
      vl_sift_detect (sift_filter); // traverse and draw each point
      const VlSiftKeypoint *keypoints_ptr =  vl_sift_get_keypoints (sift_filter);
      int num_keypoints = vl_sift_get_nkeypoints  (sift_filter); 
      // std::cout << "octave res: " << num_keypoints << "\n";
      for (int i = 0; i < num_keypoints; i++) {
        VlSiftKeypoint keyPoint = *keypoints_ptr; keypoints_ptr++;
        double orientations [4]; 
        int num_orientations = vl_sift_calc_keypoint_orientations(sift_filter, orientations, &keyPoint); 
        for (int j = 0; j < num_orientations; j++) {
          double angle = orientations [j]; // calculated for each direction of the description 
          keypoints.push_back(VKeypoint(std::make_pair(keyPoint.x, keyPoint.y), keyPoint.sigma, (float) angle, keyPoint.o, 0.0f));
          // std::cout << keyPoint.x << " " << keyPoint.y << " "  << keyPoint.sigma << " " << angle << "\n"; 
          descriptors.push_back(Eigen::RowVectorXf(128));
          vl_sift_calc_keypoint_descriptor(sift_filter, descriptors.back().data(), &keyPoint, angle);
        }
      }

      if (vl_sift_process_next_octave(sift_filter) == VL_ERR_EOF) break; // next order
    }
  }
  vl_sift_delete (sift_filter);
}

void generate_hog_keypoints(float* image, int width, int height, int num_levels, int o_min, 
  std::vector<VKeypoint>& keypoints, std::vector<Eigen::RowVectorXf>& descriptors) {
  
}

Eigen::Vector2i makePoint(int r, int c) {
  Eigen::Vector2i point;
  point[0] = r;
  point[1] = c;
  return point;
}

Eigen::Vector2i directions[8] = {
  makePoint(0,1), makePoint(-1,1), makePoint(-1,0), makePoint(-1,-1), 
  makePoint(0,-1), makePoint(1,-1), makePoint(1,0), makePoint(1,1)};

// http://users.utcluj.ro/~tmarita/IPL/IPLab/PI-L6e.pdf - border tracing
void traverseImage(
    const Eigen::MatrixXf& image, Eigen::MatrixXi& mask, 
    const Eigen::Vector2i& start, const int dir_pos,
    std::vector<std::vector<Eigen::Vector2i> >& contours,
    const Eigen::Vector2i& junction = makePoint(-1, -1)) 
{
  int cur_dir_pos = dir_pos;
  Eigen::Vector2i cur = start;
  std::vector<Eigen::Vector2i> contour;

  if (junction[0] >= 0 && junction[1] >= 0)
    contour.push_back(junction);
  contour.push_back(cur);
  mask(cur[0], cur[1]) = 1;

  std::vector<int> next_dir_pos;

  while (true) {
    int end_dir_pos = cur_dir_pos;
    bool stop = true;

    while (true) {
      int r = cur[0] + directions[cur_dir_pos][0];
      int c = cur[1] + directions[cur_dir_pos][1];
      if (r >=0 && r < image.rows() && c >= 0 && c < image.cols() && !mask(r,c)) {
        next_dir_pos.push_back(cur_dir_pos);
      }
      cur_dir_pos = (cur_dir_pos + 1)%8;
      if (cur_dir_pos == end_dir_pos) break;
    }

    if (next_dir_pos.size() == 1) {
      cur_dir_pos = next_dir_pos.back();
      next_dir_pos.pop_back();
      cur[0] += directions[cur_dir_pos][0];
      cur[1] += directions[cur_dir_pos][1];
      contour.push_back(cur);
      mask(cur[0], cur[1]) = 1;
    } else {
      break;
    }
    cur_dir_pos = (cur_dir_pos%2 == 0)  ? (cur_dir_pos+7)%8 : (cur_dir_pos+6)%8;
  }

  if (contour.size() > 2 || next_dir_pos.size() > 1) {
    if (next_dir_pos.size() > 2) {  // a junction
      mask(cur[0], cur[1]) = 2;
    }
    contours.push_back(contour);
  }

  if (next_dir_pos.size() > 1) {
    for (size_t k = 0; k < next_dir_pos.size(); ++k) {
      mask(cur[0] + directions[next_dir_pos[k]][0], cur[1] + directions[next_dir_pos[k]][1]) = 1;
    }

    for (size_t k = 0; k < next_dir_pos.size(); ++k) {
      Eigen::Vector2i next = makePoint(cur[0] + directions[next_dir_pos[k]][0], cur[1] + directions[next_dir_pos[k]][1]);
      traverseImage(image, mask, next, next_dir_pos[k], contours, cur);
    }
  }
}

void remove_vertex_with_1_edge (ContourGraph::vertex_descriptor v, ContourGraph& contour_graph, int min_num_points)
{
    std::vector<ContourGraph::edge_descriptor> out_edges;
    auto ep = boost::out_edges(v, contour_graph);
    for (auto ei = ep.first; ei != ep.second; ++ei) {
      out_edges.push_back(*ei);
    }

    if (out_edges.size() != 1) return;

    ContourGraph::edge_descriptor edge = out_edges.front();
    if (contour_graph[edge].points.size() < min_num_points) {
      boost::remove_edge(edge, contour_graph);
    }
}

void remove_vertex_with_2_edges (ContourGraph::vertex_descriptor v, ContourGraph& contour_graph)
{
    std::vector<ContourGraph::edge_descriptor> out_edges;
    auto ep = boost::out_edges(v, contour_graph);
    for (auto ei = ep.first; ei != ep.second; ++ei) {
      out_edges.push_back(*ei);
    }

    if (out_edges.size() != 2) return;

    ContourGraph::edge_descriptor edge1, edge2;
    if (contour_graph[out_edges.front()].points.back() == contour_graph[out_edges.back()].points.front()) {
      edge1 = out_edges.front();
      edge2 = out_edges.back();
    } else if (contour_graph[out_edges.back()].points.back() == contour_graph[out_edges.front()].points.front()) {
      edge2 = out_edges.front();
      edge1 = out_edges.back();
    } else {
      std::cout << "could not merge\n"; 
      return;
    }

    std::vector<Eigen::Vector2i> new_contour = contour_graph[edge1].points;
    new_contour.insert(new_contour.end(), contour_graph[edge2].points.begin()+1, contour_graph[edge2].points.end());

    auto new_edge = boost::add_edge(
      boost::target(edge1, contour_graph), boost::target(edge2, contour_graph), contour_graph);
    contour_graph[new_edge.first].points = new_contour;

    boost::remove_edge(boost::target(edge1, contour_graph), v, contour_graph);
    boost::remove_edge(v, boost::target(edge2, contour_graph), contour_graph);
}

void generate_contour_graph(const Eigen::MatrixXf& image, ContourGraph& contour_graph) {
  std::vector<std::vector<Eigen::Vector2i> > contours;

  std::vector<Eigen::Vector2i> candidates;
  Eigen::MatrixXi mask = Eigen::MatrixXi::Zero(image.rows(), image.cols());
  for (int r = 0; r < image.rows(); ++r) {
    for (int c = 0; c < image.cols(); ++c) {
      if (image(r, c) > 0) {
        mask(r,c) = 1;
      } else {
        candidates.push_back(makePoint(r, c));
      }
    }
  }

  for (int r = 0; r < image.rows(); ++r) {
    for (int c = 0; c < image.cols(); ++c) {
      if (image(r, c) == 0) {
        int num_neighbours = 0;
        int dir_pos = -1;
        for (size_t k = 0; k < 8; ++k) {
          int nr = r + directions[k][0];
          int nc = c + directions[k][1];
          if (nr >=0 && nc >= 0 && nr < image.rows() && nc < image.cols() && image(nr, nc) == 0) {
            ++num_neighbours;
            dir_pos = k;
          }
        }
        if (num_neighbours == 1) {
          for (size_t k = 2; k <= 6; ++k) {
            int nr = r + directions[(dir_pos+k)%8][0];
            int nc = c + directions[(dir_pos+k)%8][1];
            if (mask(nr, nc) == 1) {
              for (size_t k2 = 2; k2 < 6; ++k2) {
                int nnr = nr + directions[(dir_pos+k2)%8][0];
                int nnc = nc + directions[(dir_pos+k2)%8][1];
                if (nnr >=0 && nnc >= 0 && nnr < image.rows() && nnc < image.cols() && image(nnr, nnc) == 0) {
                  mask(nr, nc) = 0; 
                }
              }
            }
          }
        }
      }
    }
  }

  for (size_t i = 0; i < candidates.size(); ++i) {
    if (mask(candidates[i][0], candidates[i][1])) continue;
    traverseImage(image, mask, candidates[i], 7, contours);
  }

  std::map<std::pair<int,int>, ContourGraph::vertex_descriptor> node_descriptors;

  std::set<std::pair<int,int> > endpoints;
  for (size_t k = 0; k < contours.size(); ++k) {
    endpoints.insert(std::make_pair(contours[k].front()[0], contours[k].front()[1]));
    endpoints.insert(std::make_pair(contours[k].back()[0], contours[k].back()[1]));
  }

  for (std::set<std::pair<int,int> >::iterator it = endpoints.begin(); it != endpoints.end(); ++it) {
    ContourGraph::vertex_descriptor v = boost::add_vertex(contour_graph);
    contour_graph[v].id = node_descriptors.size();
    contour_graph[v].point = Eigen::Vector2i(it->first, it->second);
    node_descriptors[*it] = v;
  }

  for (size_t k = 0; k < contours.size(); ++k) {
    std::pair<int, int> start(contours[k].front()[0], contours[k].front()[1]);
    std::pair<int, int> end(contours[k].back()[0], contours[k].back()[1]);
    std::pair<ContourGraph::edge_descriptor, bool> edge = boost::add_edge(node_descriptors[start], node_descriptors[end], contour_graph);
    contour_graph[edge.first].points = contours[k];
    if (!edge.second) {
      std::cout << "Contour was already added between (" << start.first << "," << start.second << ") and (" << end.first << "," << end.second << ")\n"; 
    }
  }

    while (true) {
      int prev_num_edges = boost::num_edges(contour_graph); 
      while (true) {
        int num_edges = boost::num_edges(contour_graph);   
        for (auto vp = boost::vertices(contour_graph); vp.first != vp.second; ++vp.first) {
          if (boost::out_degree(*vp.first, contour_graph) == 2) 
            remove_vertex_with_2_edges(*vp.first, contour_graph);
        }
        std::vector<ContourGraph::vertex_descriptor> isolated_vertices;
        for (auto vp = boost::vertices(contour_graph); vp.first != vp.second; ++vp.first) {
          if (boost::out_degree(*vp.first, contour_graph) == 0) {
            isolated_vertices.push_back(*vp.first);
          } 
        }
        for (size_t i = 0; i < isolated_vertices.size(); ++i) boost::remove_vertex(isolated_vertices[i], contour_graph);
        if (num_edges == boost::num_edges(contour_graph)) break;
      }
      while (true) {
        int num_edges = boost::num_edges(contour_graph);
        for (auto vp = boost::vertices(contour_graph); vp.first != vp.second; ++vp.first) {
          if (boost::out_degree(*vp.first, contour_graph) == 1) {
            remove_vertex_with_1_edge(*vp.first, contour_graph, 5);
          } 
        }
        std::vector<ContourGraph::vertex_descriptor> isolated_vertices;
        for (auto vp = boost::vertices(contour_graph); vp.first != vp.second; ++vp.first) {
          if (boost::out_degree(*vp.first, contour_graph) == 0) {
            isolated_vertices.push_back(*vp.first);
          } 
        }
        for (size_t i = 0; i < isolated_vertices.size(); ++i) boost::remove_vertex(isolated_vertices[i], contour_graph);
        if (num_edges == boost::num_edges(contour_graph)) break;
      }
      if (prev_num_edges == boost::num_edges(contour_graph)) break;
    }
}

void generate_custom_keypoints(float* image, int width, int height, int num_levels, int o_min, 
    std::vector<VKeypoint>& keypoints, std::vector<Eigen::RowVectorXf>& descriptors) 
{
  Eigen::MatrixXf img = Eigen::Map<Eigen::MatrixXf>(image, width, height).transpose();

  ContourGraph contour_graph;
  // generate_contour_graph(img, contour_graph);

  /*std::vector<unsigned short> tmp(img.rows()*img.cols(), 65355);
  typedef boost::graph_traits<ContourGraph>::edge_iterator edge_iter;
  edge_iter ei, ei_end;
  int cur_edge = 0;
  for (boost::tie(ei, ei_end) = boost::edges(contour_graph); ei != ei_end; ++ei) {
    const std::vector<Eigen::Vector2i>& contour = contour_graph[*ei].points;
    for (size_t i = 0; i < contour.size(); ++i) {
      int r = contour[i][0];
      int c = contour[i][1];
      tmp[(img.rows() - 1 - r)*img.cols() + c] = (cur_edge*1.0/(boost::num_edges(contour_graph)-1))*55000;
    }
    ++cur_edge;
  }
  pcl::io::saveShortPNGFile("test.png", &tmp[0], img.cols(), img.rows(), 1);*/

  VlSiftFilt * filt = 0 ;
  void * descr = 0 ;
  filt = vl_sift_new (width, height, -1, -1, 0);
  float* grad = new float[2 *width * height];
  vl_imgradient_polar_f (grad, grad +1, 2, 2 * width, image, width, height, width) ;

  if (boost::num_vertices(contour_graph) == 0)
  {
    std::vector<std::pair<int,int> > candidates;
    for (int r = 0; r < img.rows(); ++r) {
      for (int c = 0; c < img.cols(); ++c) {
        if (img(r, c) > 0) {
        } else {
          candidates.push_back(std::make_pair(r, c));
        }
      }
    }

    int ideal = 100;
    int incr = std::ceil(candidates.size()*1.0f/ideal);
    int i = 0;

    for (size_t k = 0; k < candidates.size(); k += incr) {
      ContourGraph::vertex_descriptor v = boost::add_vertex(contour_graph);
      contour_graph[v].id = i;
      contour_graph[v].point = Eigen::Vector2i(candidates[k].first, candidates[k].second);
      ++i;
    }
  }

  std::cout << "Num of junctions: " << boost::num_vertices(contour_graph) << "\n";
  std::cout << "Num of contours: " << boost::num_edges(contour_graph) << "\n";

  for (auto vp = boost::vertices(contour_graph); vp.first != vp.second; ++vp.first) {
    Eigen::Vector2i point = contour_graph[*vp.first].point;
    int r = point[0], c = point[1];

    VlSiftKeypoint vl_keypoint;
    vl_sift_keypoint_init (filt, &vl_keypoint, c, r, std::max(width, height)/5);
    keypoints.push_back(VKeypoint(std::make_pair(vl_keypoint.x, vl_keypoint.y), vl_keypoint.sigma, 0.0f, vl_keypoint.o, 0.0f)); 
    descriptors.push_back(Eigen::RowVectorXf::Zero(128));
    vl_sift_calc_raw_descriptor (filt, grad, descriptors.back().data(), width, height, c, r, vl_keypoint.sigma, 0.0f);
  }
  delete grad;
  vl_sift_delete (filt);
  std::cout << "num keypoints: " << keypoints.size() << "\n";
}

int main(int argc, char **argv) {
  // Parse cmd flags
  std::map<std::string, std::string> flags;
  parseCmdArgs(argc, argv, &flags);
  bool arg_error = false;

  std::string image_filename, image_filelist;
  std::string keypoints_filename, keypoints_filelist;
  std::string output_filelist, output_prefix;
  std::string feature_type;
  if (!(getFlag(flags, "image", "",  &image_filename) ^
        getFlag(flags, "image_filelist", "",  &image_filelist))) {
    printf("Error: specify --image xor --image_filelist\n");
    arg_error = true;
  }
  if (image_filelist.size() > 0 &&
      !(getFlag(flags, "output_filelist", "",  &output_filelist) && 
        getFlag(flags, "keypoints_filelist", "", &keypoints_filelist))) {
    printf("Error: specify --keypoints_filelist and --output_filelist\n");
    arg_error = true;
  }
  if (image_filename.size() > 0 && 
      !getFlag(flags, "keypoints", "", &keypoints_filename)) {
    printf("Error: specify --keypoints\n");
    arg_error = true;
  }
  getFlag(flags, "output_prefix", "",  &output_prefix);
  getFlag(flags, "feature_type", "",  &feature_type);

  bool verbose;
  getFlag(flags, "verbose", false, &verbose);

  if (arg_error)   return 1;

  int num_levels = 3;

  std::map<std::string, std::string> id_to_image_filenames;
  if (image_filename.size() > 0 && keypoints_filename.size() > 0) {
    getMapFromFilename(image_filename, id_to_image_filenames);
  } else if (image_filelist.size() > 0 && keypoints_filelist.size() > 0) {
    getMapFromFilelist(image_filelist, id_to_image_filenames);
  }

  std::map<std::string, std::string>::iterator image_filename_it = id_to_image_filenames.begin(); 

  std::ofstream filelist_out(output_filelist.c_str());
  std::ofstream keypts_filelist_out(keypoints_filelist.c_str());

  for  (; image_filename_it != id_to_image_filenames.end(); ++image_filename_it) {
    std::string image_id = image_filename_it->first;
    std::string image_filename = image_filename_it->second;
    if (verbose)
      pcl::console::print_highlight ("Process %s: %s\n", 
        image_filename_it->first.c_str(), image_filename_it->second.c_str());
    
    // Open image and extract features
    std::vector<VKeypoint> keypoints;
    std::vector<Eigen::RowVectorXf> descriptors;
    if (image_filename_it == id_to_image_filenames.end()){
      printf("Error: could not find image filename corresponding to: %s\n", image_id.c_str());
      continue;
    }

    vtkSmartPointer<vtkImageData> image_data;
    vtkSmartPointer<vtkPNGReader> reader = vtkSmartPointer<vtkPNGReader>::New ();
    reader->SetFileName (image_filename.c_str());
    image_data = reader->GetOutput ();
    reader->Update();
    // image_data->Update ();
    int dimensions[3];
    image_data->GetDimensions (dimensions);
    
    int width = dimensions[0], height = dimensions[1], components = image_data->GetNumberOfScalarComponents();
    float* image = new float[width*height*components];
    for (int y = 0; y < height; ++y) { // rows
      for (int x = 0; x < width; ++x) { // cols
        for (int k = 0; k < components; ++k) { // channels
            image[x + width*y + width*height*k] = image_data->GetScalarComponentAsFloat(x, y, 0, k);
        }
      }
    }
    
    if (feature_type == "sift") {
      generate_sift_keypoints(image, width, height, num_levels, 0, keypoints, descriptors);
      if (keypoints.size() == 0) generate_sift_keypoints(image, width, height, num_levels, -1, keypoints, descriptors);
    } else if (feature_type == "custom") {
      generate_custom_keypoints(image, width, height, num_levels, 0, keypoints, descriptors);
    } else {
      std::cerr << "Error: feature type not supported: " << feature_type << "\n";
    }
    
    delete [] image;

    std::stringstream prefix_ss;
    prefix_ss << output_prefix << image_id << "_" << feature_type;
    std::string descriptor_filename = prefix_ss.str() + ".txt";
    std::string keypoints_filename = prefix_ss.str() + "_pts.txt";
    std::string keypoints_img_filename = prefix_ss.str() + "_pts.png";

    if (descriptors.size() == 0) std::cout << "Error: no keypoints found in " << image_filename << "\n";
    Eigen::MatrixXf image_descriptors(descriptors.size(), (descriptors.size()) ? descriptors[0].size() : 0);
    for (size_t i = 0; i < descriptors.size(); ++i) image_descriptors.row(i) = descriptors[i];
    saveEigenMatrix(descriptor_filename, image_descriptors);

    std::ofstream keypoints_ofs(keypoints_filename.c_str());
    for (size_t k = 0; k < keypoints.size(); ++k)
      keypoints_ofs << keypoints[k].point.first << " " << keypoints[k].point.second << " " 
                    << keypoints[k].size << " "<< keypoints[k].angle << " "
                    << keypoints[k].octave << " "<< keypoints[k].response << "\n";
    keypoints_ofs.close();

    // Draw a circle in the center of the image
    vtkSmartPointer<vtkImageCanvasSource2D> drawing = 
      vtkSmartPointer<vtkImageCanvasSource2D>::New();
    drawing->SetNumberOfScalarComponents(1);
    drawing->SetScalarTypeToUnsignedShort();
    drawing->SetExtent(image_data->GetExtent());
    drawing->SetDrawColor(0.0);
    drawing->FillBox(image_data->GetExtent()[0], image_data->GetExtent()[1],
                     image_data->GetExtent()[2], image_data->GetExtent()[3]);
    drawing->SetDrawColor(32535.0);
    for (size_t k = 0; k < keypoints.size(); ++k)
      drawing->DrawCircle(keypoints[k].point.first, keypoints[k].point.second, keypoints[k].size/2.0);
    // Combine the images (blend takes multiple connections on the 0th
    // input port)
    vtkSmartPointer<vtkImageBlend> blend = 
      vtkSmartPointer<vtkImageBlend>::New();
    blend->AddInputConnection(reader->GetOutputPort());
    blend->AddInputConnection(drawing->GetOutputPort());
    blend->SetOpacity(0,.6);
    blend->SetOpacity(1,.4);
    // Cast
    vtkSmartPointer<vtkImageCast> castFilter =
    vtkSmartPointer<vtkImageCast>::New();
    castFilter->SetOutputScalarTypeToUnsignedShort ();
    castFilter->SetInputConnection(blend->GetOutputPort());
    castFilter->Update();
    // Save
    vtkSmartPointer<vtkPNGWriter> writer =
      vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFileName(keypoints_img_filename.c_str());
    writer->SetInputConnection(castFilter->GetOutputPort());
    writer->Write();

    if (output_filelist.size() > 0)
      filelist_out << image_id << " " << descriptor_filename << "\n";
    if (keypoints_filelist.size() > 0)
      keypts_filelist_out << image_id << " " << keypoints_filename << "\n";
    // break;
  }

  filelist_out.close();
  keypts_filelist_out.close();
  return 0;
}
