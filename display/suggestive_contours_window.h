#ifndef SUGGESTIVE_CONTOURS_WINDOW_H
#define SUGGESTIVE_CONTOURS_WINDOW_H

#include "display/pcl_window.h"

namespace trimesh {
  class TriMesh;
  template<size_t D, class T>
  class Vec;
  typedef Vec<2ul, float> vec2;
}

class SuggestiveContoursWindow : public PCLWindow {
public:
  SuggestiveContoursWindow(pcl::PolygonMesh *mesh,
    int width, int height, 
    int posX, int posY, 
    const char* title);

  virtual ~SuggestiveContoursWindow();

  virtual void initShaders();

  virtual void draw();

  bool grabContoursImage (
    const GLCamera& camera, float *data, int width, int height);

private:
  trimesh::TriMesh *trimesh_;
};

#endif // SUGGESTIVE_CONTOURS_WINDOW_H