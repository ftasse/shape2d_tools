#ifndef SILHOUETTE_WINDOW_H
#define SILHOUETTE_WINDOW_H

#include "display/pcl_window.h"

class NaiveSilhouetteWindow : public PCLWindow {
public:
  NaiveSilhouetteWindow(pcl::PolygonMesh *mesh,
    int width, int height, 
    int posX, int posY, 
    const char* title) : PCLWindow(mesh, width, height, posX, posY, title) {}

  virtual ~NaiveSilhouetteWindow() {}

  virtual void initShaders();

  virtual void draw();
};

class SilhouetteWindow : public PCLWindow {
// ref: http://ogldev.atspace.co.uk/www/tutorial39/tutorial39.html
public:
  SilhouetteWindow(pcl::PolygonMesh *mesh,
    int width, int height, 
    int posX, int posY, 
    const char* title) : PCLWindow(mesh, width, height, posX, posY, title) {}

  virtual ~SilhouetteWindow() {}

  virtual void initShaders();

  virtual void draw();

  virtual void updateTopology() ;
};

bool geometryShaderSupported();

#endif // SILHOUETTE_WINDOW_H