#include "display/suggestive_contours_window.h"

#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#ifdef _WIN32
  #include <windows.h>
#endif
#include <GL/gl.h>
#endif

#include <pcl/conversions.h>
#include <pcl/io/vtk_lib_io.h>

#include "trimesh2/include/trimesh.h"

void convertPCLMeshToTriMesh (pcl::PolygonMesh* mesh_, trimesh::TriMesh* trimesh_) {
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_(
    new pcl::PointCloud<pcl::PointXYZRGB> ());
  pcl::PointCloud<pcl::Normal>::Ptr normals_(
    new pcl::PointCloud<pcl::Normal> ());
  pcl::fromPCLPointCloud2(mesh_->cloud, *cloud_);
  pcl::fromPCLPointCloud2(mesh_->cloud, *normals_);

  trimesh_->vertices.resize(cloud_->size());
  trimesh_->normals.resize(normals_->size());
  trimesh_->colors.resize(cloud_->size());

  for (size_t i = 0; i < cloud_->size(); ++i) {
    trimesh_->vertices[i][0]  = cloud_->points[i].x;
    trimesh_->vertices[i][1]  = cloud_->points[i].y;
    trimesh_->vertices[i][2]  = cloud_->points[i].z;

    trimesh_->normals[i][0]  = normals_->points[i].normal_x;
    trimesh_->normals[i][1]  = normals_->points[i].normal_y;
    trimesh_->normals[i][2]  = normals_->points[i].normal_z;

    trimesh_->colors[i] = trimesh::Color(cloud_->points[i].r, cloud_->points[i].g, cloud_->points[i].b);
  }

  std::vector<int> thisface;
  for (size_t i = 0; i < mesh_->polygons.size(); i++) {
    thisface = std::vector<int>(mesh_->polygons[i].vertices.begin(), mesh_->polygons[i].vertices.end());
    if (thisface.size() < 3) {
      continue;
    } else if (thisface.size() == 3) {
      trimesh_->faces.push_back(
        trimesh::TriMesh::Face(thisface[0], thisface[1], thisface[2]));
    } else {
      for (size_t k = 2; k < thisface.size(); k++)
        trimesh_->faces.push_back(trimesh::TriMesh::Face(thisface[0],
                   thisface[i-1],
                   thisface[i]));
    }
  }
}

SuggestiveContoursWindow::SuggestiveContoursWindow(pcl::PolygonMesh *mesh,
    int width, int height, 
    int posX, int posY, 
    const char* title) : PCLWindow(mesh, width, height, posX, posY, title), trimesh_(NULL) 
{
  // trimesh_ = new trimesh::TriMesh();
  // convertPCLMeshToTriMesh(mesh_, trimesh_);
}

SuggestiveContoursWindow::~SuggestiveContoursWindow() { 
  if (trimesh_) delete trimesh_; 
}

void SuggestiveContoursWindow::initShaders() {
}

void SuggestiveContoursWindow::draw() {
  
}

bool SuggestiveContoursWindow::grabContoursImage (const GLCamera& camera, float *data, int width, int height) {
  std::string basename = tmpnam(NULL);
  std::string mesh_filename =  basename + ".ply";
  std::string image_filename =  basename + ".pgm";
  std::string xf_filename =  basename + ".xf";
  // trimesh_->write(mesh_filename.c_str());
  pcl::io::savePolygonFile(mesh_filename.c_str(), *mesh_);

  glm::mat4 xf = camera.view_matrix();
  std::ofstream xf_ofs(xf_filename.c_str());
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      xf_ofs << xf[j][i]; //[i+4*j];
      if (j == 3) xf_ofs << ::std::endl;
      else xf_ofs << " ";
    }
  }
  xf_ofs.close();

  char command[512];  //key for subdivision: (1-'a')+'s'
  sprintf (command, "$( cd \"$( dirname \"${BASH_SOURCE[0]}\" )\" && pwd )/extern/rtsc/rtsc -fA%cs +%d,%d,0.01,0.04 %s %s", (1-'a')+'s', width, height+32, mesh_filename.c_str(), image_filename.c_str());   
  printf("Call: %s\n", command);
  std::system(command);

  FILE *f = fopen(image_filename.c_str(), "rb");
  char magic_number[3];
  int img_width, img_height, img_max_val;
  fscanf(f, "%s\n%d %d\n%d\n", magic_number, &img_width, &img_height, &img_max_val);
  if (strcmp(magic_number, "P5")!=0 || img_width != width || img_height != height) {
    printf("Cannot grab contours image: %s\n%d %d\n%d\n", magic_number, img_width, img_height, img_max_val);
  } else {
    unsigned char *buf = new unsigned char[width*height];
    fread(buf, width*height, 1, f);
    unsigned char minval = 255, maxval = 0;
    for (size_t i = 0; i < width*height; ++i) {
      minval = std::min(minval, buf[i]);
      maxval = std::max(maxval, buf[i]);
    }

    if (minval == maxval) {
      fclose(f);
      sprintf (command, "$( cd \"$( dirname \"${BASH_SOURCE[0]}\" )\" && pwd )/extern/rtsc/rtsc -fbAs +%d,%d,0.01,0.04 %s %s", width, height+32, mesh_filename.c_str(), image_filename.c_str());   
      printf("Call: %s\n", command);
      std::system(command);
      f = fopen(image_filename.c_str(), "rb");
      fscanf(f, "%s\n%d %d\n%d\n", magic_number, &img_width, &img_height, &img_max_val);
      fread(buf, width*height, 1, f);
    }

    for (size_t i = 0; i < width*height; ++i) 
      if (buf[i] < 250) data[i] = 0; else data[i] = 1.0;
    delete buf;
  }
  fclose(f);

  std::remove(mesh_filename.c_str());
  std::remove(image_filename.c_str());
  std::remove(xf_filename.c_str());
  return true;
}
