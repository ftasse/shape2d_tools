#include "display/silhouette_window.h"

#include <map>
#include <GL/glew.h>
#include <pcl/conversions.h>

#include "3d_tools/display/glew_utils.h"

bool geometryShaderSupported() {
  if (GLEW_VERSION_3_1) {
    std::cout << "OpenGL 3.1 is supported.\n";
    if (GLEW_VERSION_4_2) {
      std::cout << "OpenGL 4.2 is supported.\n";
      return false;
    }
    else {
      std::cout << "Alert: OpenGL 4.2 is not supported.\n";
    }
  }
  return true;
}

void NaiveSilhouetteWindow::initShaders() {
  shader_program_ = create_shader_program("shaders/vs.glsl", "shaders/silhouette_fs.glsl");
  pos_attr_id_ = bindAttributeVariable(shader_program_, "position");
  transform_uniform_id_ = bindUniformVariable(shader_program_, "vp_tranform");
  glUseProgram(shader_program_);
}

void NaiveSilhouetteWindow::draw() {
  if (mesh_ == NULL)  return;

  glPushAttrib( GL_ALL_ATTRIB_BITS );
  glLineWidth(2.0);
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glEnable(GL_POLYGON_OFFSET_FILL);
  // glPolygonOffset(-2.5, -2.5f);
  // draw solid objects
  PCLWindow::draw();
  glDepthMask(GL_FALSE);
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glColor4f(foreground_color[0], foreground_color[1], foreground_color[2], foreground_color[3]);
  glDisable(GL_POLYGON_OFFSET_FILL);
  glPolygonMode(GL_BACK, GL_LINE);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);
  // draw solid objects again 
  PCLWindow::draw();
  // draw true edges           // for a complete hidden-line drawing
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDepthMask(GL_TRUE);
  glDisable(GL_CULL_FACE);
  
  glPopAttrib();
}

void SilhouetteWindow::initShaders() {
  std::vector<char*> feedback_varyings (1, (char*) "g_position");
  shader_program_ = create_shader_program(
    "shaders/vs.glsl", "shaders/silhouette_fs.glsl", "shaders/silhouette_gs.glsl", feedback_varyings);
  pos_attr_id_ = bindAttributeVariable(shader_program_, "position");
  transform_uniform_id_ = bindUniformVariable(shader_program_, "vp_tranform");
  glUseProgram(shader_program_);
}

void SilhouetteWindow::draw() {
  glUniformMatrix4fv(transform_uniform_id_, 1, GL_FALSE, &camera()->matrix()[0][0]);
  glBindVertexArray(vao_);

  if (index_vbo_ > 0) {
    glEnable(GL_PRIMITIVE_RESTART);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo_); 
    glDrawElements(
      GL_TRIANGLES_ADJACENCY,      // mode
      num_indices_,    // count
      GL_UNSIGNED_INT,   // type
      (void*)0           // element array buffer offset
    );
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisable(GL_PRIMITIVE_RESTART);
  }

  glBindVertexArray(0);
}

void SilhouetteWindow::updateTopology() {
  if (mesh_ == NULL) {
    num_indices_ = 0;
    return;
  }

  unsigned int primitive_restart_index = -1;

  std::map<std::pair<int,int>, std::vector<int> > edge_face_adjacency;
  for (size_t f = 0 ; f < mesh_->polygons.size() ; f++) {
    if (mesh_->polygons[f].vertices.size() != 3) continue;
    for (size_t k = 0; k < mesh_->polygons[f].vertices.size(); ++k) {
      std::pair<int, int> edge(
        mesh_->polygons[f].vertices[k],
        mesh_->polygons[f].vertices[(k+1)%3]);
      if (edge.first > edge.second) edge = std::make_pair(edge.second, edge.first);
      edge_face_adjacency[edge].push_back(f);
    }
  }

  std::vector<unsigned int> indices; 
  indices.reserve(6*mesh_->polygons.size());

  for (size_t f = 0 ; f < mesh_->polygons.size() ; f++) {
    if (mesh_->polygons[f].vertices.size() != 3) continue;
    std::vector<unsigned int> triangle_adj_indices;
    triangle_adj_indices.reserve(6);

    for (size_t k = 0; k < mesh_->polygons[f].vertices.size(); ++k) {
      std::pair<int, int> edge(
        mesh_->polygons[f].vertices[k],
        mesh_->polygons[f].vertices[(k+1)%3]);
      if (edge.first > edge.second) edge = std::make_pair(edge.second, edge.first);
      std::vector<int>& edge_faces =  edge_face_adjacency[edge];

      int other_f = -1;
      for (size_t l = 0; l < edge_faces.size(); ++l) {
        if (edge_faces[l] != f) {
          other_f = edge_faces[l];
          break;
        }
      }
      if (other_f == -1) break; // do not add face to list of indices

      int opposite_index = -1;
      for (size_t l = 0; l < mesh_->polygons[other_f].vertices.size(); ++l) {
        if (mesh_->polygons[other_f].vertices[l] != edge.first && 
            mesh_->polygons[other_f].vertices[l] != edge.second)  {
          opposite_index = mesh_->polygons[other_f].vertices[l];
          break;
        }
      }
      if (opposite_index == -1) break; // do not add face to list of indices

      triangle_adj_indices.push_back(mesh_->polygons[f].vertices[k]);
      triangle_adj_indices.push_back(opposite_index);
    }

    if (triangle_adj_indices.size() == 6) {
      indices.insert(indices.end(), triangle_adj_indices.begin(), triangle_adj_indices.end());
    }  else if (GLEW_VERSION_3_1)
      indices.push_back(primitive_restart_index);
  }

  num_indices_ = indices.size();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_vbo_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_indices_ * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glPrimitiveRestartIndex (primitive_restart_index);

  std::cout << "num_indices: " << primitive_restart_index << " " << mesh_->polygons.size() << " " << num_indices_ << "\n";
}

