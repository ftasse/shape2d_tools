layout (triangles_adjacency) in;
layout (line_strip, max_vertices = 6) out;

in vec3 world_position[];

in vec4 f_color[];
in vec3 f_normal[];
in vec3 f_position[];
out vec4 g_color;
out vec3 g_normal;
out vec3 g_position;

void EmitLine(int StartIndex, int EndIndex) {
    gl_Position = gl_in[StartIndex].gl_Position;
    g_color = f_color[StartIndex];
    g_normal = f_normal[StartIndex];
    g_position = f_position[EndIndex];
    EmitVertex();

    gl_Position = gl_in[EndIndex].gl_Position;
    g_color = f_color[EndIndex];
    g_normal = f_normal[EndIndex];
    g_position = f_position[EndIndex];
    EmitVertex();

    EndPrimitive();
}

// uniform vec3 light_pos;
vec3 light_pos  = vec3(0.0, 1.0, 1.0);

void main(void) {
    vec3 e1 = world_position[2] - world_position[0];
    vec3 e2 = world_position[4] - world_position[0];
    vec3 e3 = world_position[1] - world_position[0];
    vec3 e4 = world_position[3] - world_position[2];
    vec3 e5 = world_position[4] - world_position[2];
    vec3 e6 = world_position[5] - world_position[0];

    vec3 normal = cross(e1,e2);
    vec3 light_dir = light_pos - world_position[0];

    if (dot(normal, light_dir) > 0.00001) {

        normal = cross(e3,e1);

        if (dot(normal, light_dir) <= 0) {
            EmitLine(0, 2);
        }

        normal = cross(e4,e5);
        light_dir = light_pos - world_position[2];

        if (dot(normal, light_dir) <=0) {
            EmitLine(2, 4);
        }

        normal = cross(e2,e6);
        light_dir = light_pos - world_position[4];

        if (dot(normal, light_dir) <= 0) {
            EmitLine(4, 0);
        }
    }
}