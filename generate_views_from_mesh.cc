#include <float.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdlib>

// #include <omp.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#ifdef _WIN32
  #include <windows.h>
#endif
#include <GL/gl.h>
#endif

#include <Eigen/Eigenvalues>

#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/norm.hpp>

#include <pcl/common/time.h>
#include <pcl/point_types.h>
#include <pcl/common/centroid.h>
#include <pcl/search/kdtree.h>
#include <pcl/PolygonMesh.h>
#include <pcl/io/png_io.h>
#include <pcl/common/pca.h>
#include <pcl/common/common.h>
#include <pcl/features/normal_3d_omp.h>

#include "3d_tools/utils/cmd_parser.h"
#include "3d_tools/features/normals.h"
#include "3d_tools/io/off_io.h"
#include "3d_tools/utils/pcl_utils.h"
#include "3d_tools/display/gl_manager.h"
#include "display/pcl_window.h"
#include "display/silhouette_window.h"
#include "display/suggestive_contours_window.h"


bool lessVec3(const glm::vec3& v, const glm::vec3& w) {
  /*if (v[0] < w[0])  return true;
  else if (w[0] > v[0]) return false;
  if (v[1] < w[1])  return true;
  else if (w[1] > v[1]) return false;
  if (v[2] < w[2])  return true;
  else if (w[2] > v[2]) return false;
  return false;*/
  return std::make_pair(std::make_pair(v[0], v[1]), v[2]) < std::make_pair(std::make_pair(w[0], w[1]), w[2]);
}

bool equalVec3(const glm::vec3& v, const glm::vec3& w) {
  return ( std::abs(v[0] - w[0]) < FLT_MIN && std::abs(v[1] - w[1]) < FLT_MIN && std::abs(v[2] - w[2]) < FLT_MIN);
}

void fibonacci_sphere(std::vector<glm::vec3>& points, int num_samples = 1, bool randomize=true) {
  int rnd = 1;
  if (randomize) rnd = rand()%num_samples;

  float offset = 2.0f/num_samples;
  float increment = M_PI * (3.0f - sqrt(5.0f));
  points.clear();

  for (size_t i = 0; i < num_samples; ++i) {
    float y = ((i * offset) - 1) + (offset / 2);
    float r = sqrt(1 - pow(y,2));
    float phi = ((i + rnd) % num_samples) * increment;
    float x = cos(phi) * r;
    float z = sin(phi) * r;
    points.push_back(glm::vec3(x,y,z));
  }
}

// Buggy: duplicate points
void octahedron_sphere(std::vector<glm::vec3>& points, int num_levels = 1) {
  typedef struct {
    glm::vec3 points[3];
  } triangle;

  glm::vec3 xplus (1,  0,  0);
  glm::vec3 xmin (-1,  0,  0);
  glm::vec3 yplus (0,  1,  0);
  glm::vec3 ymin (0, -1,  0);
  glm::vec3 zplus (0,  0,  1);
  glm::vec3 zmin (0,  0, -1);

  /* Vertices of a unit octahedron */
  triangle octahedron[] = {
      { xplus, zplus, yplus },
      { yplus, zplus, xmin  },
      { xmin , zplus, ymin  },
      { ymin , zplus, xplus },
      { xplus, yplus, zmin  },
      { yplus, xmin , zmin  },
      { xmin , ymin , zmin  },
      { ymin , xplus, zmin  }
  };

  std::vector<triangle> cur_subdivision(octahedron, octahedron + 8);
  for (size_t level = 1; level < num_levels; level++) {
    std::vector<triangle> new_subdivision;
    new_subdivision.reserve(cur_subdivision.size()*4);

    for (size_t t = 0; t < cur_subdivision.size(); ++t) {
      glm::vec3 mid01 = glm::normalize(0.5f*(cur_subdivision[t].points[0] + cur_subdivision[t].points[1]));
      glm::vec3 mid12 = glm::normalize(0.5f*(cur_subdivision[t].points[1] + cur_subdivision[t].points[2]));
      glm::vec3 mid02 = glm::normalize(0.5f*(cur_subdivision[t].points[2] + cur_subdivision[t].points[0]));
      triangle tri0 = {cur_subdivision[t].points[0], mid01, mid02};
      triangle tri1 = {mid01, cur_subdivision[t].points[1], mid12};
      triangle tri2 = {mid02, mid01, mid12};
      triangle tri3 = {mid02, mid12, cur_subdivision[t].points[2]};
      new_subdivision.push_back(tri0);
      new_subdivision.push_back(tri1);
      new_subdivision.push_back(tri2);
      new_subdivision.push_back(tri3);
    }

    cur_subdivision = new_subdivision;
  }

  for (size_t t = 0; t < cur_subdivision.size(); ++t) {
    points.push_back(cur_subdivision[t].points[0]);
    points.push_back(cur_subdivision[t].points[1]);
    points.push_back(cur_subdivision[t].points[2]);
  }
  std::sort (points.begin(), points.end(), lessVec3);
  std::vector<glm::vec3>::iterator it = std::unique (points.begin(), points.end(), equalVec3);
  points.resize(std::distance(points.begin(),it) );
  // for (size_t i = 0; i < points.size(); ++i)
  //  std::cout << i << " : " << points[i][0] << " " << points[i][1] << " " << points[i][2] << "\n";
}

bool saveDepthMap(const std::string& map_filename, const Eigen::MatrixXf& map);

void generate_depthmaps(
  pcl::PolygonMesh& mesh, std::vector<Eigen::MatrixXf>& depthmaps, int extract_silhouettes, int window_width, size_t num_levels, size_t num_maps, bool use_random, bool verbose) 
{
  srand(time(NULL));
  bool use_suggestive_contours = (extract_silhouettes == 2);
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

  pcl::PointCloud<pcl::PointXYZ>::Ptr points (
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::Normal>::Ptr normals (
      new pcl::PointCloud<pcl::Normal>);
  pcl::fromPCLPointCloud2(mesh.cloud, *points);

  float scale_factor = getNormalizingScaleFactor(points, true); scale_factor = 1.0;
  float diameter = 2.0*(1.0/scale_factor); 

  if (num_maps != 1) {
    pcl::PCA<pcl::PointXYZ> pca;
    pca.setInputCloud(points);
    pcl::PointCloud<pcl::PointXYZ>::Ptr projected (
        new pcl::PointCloud<pcl::PointXYZ>);
    pca.project(*points, *projected);
    *points = *projected;
  }

  pcl::toPCLPointCloud2(*points, mesh.cloud);
  computeNormalsIfNecessary(mesh, points, normals, 18, tree, true, true);
  
  bool recompute_normals = num_maps==1?false:true;

  if (recompute_normals) {
    int voteCount=0;
    bool flip_face_orientation = false;
    float angleThreshold = cos(60.0f*M_PI/180.0f);
    std::vector<int> minVertVec;
    std::vector<int> maxVertVec;
    std::vector<Eigen::Vector3f> dirVec;
    dirVec.push_back(Eigen::Vector3f(1,0,0));
    dirVec.push_back(Eigen::Vector3f(0,1,0));
    dirVec.push_back(Eigen::Vector3f(0,0,1));
    dirVec.push_back(Eigen::Vector3f( 1, 1,1));
    dirVec.push_back(Eigen::Vector3f(-1, 1,1));
    dirVec.push_back(Eigen::Vector3f(-1,-1,1));
    dirVec.push_back(Eigen::Vector3f( 1,-1,1));
    for(size_t k=0;k<dirVec.size();++k) {
      dirVec[k] = dirVec[k].normalized();
      minVertVec.push_back(0);
      maxVertVec.push_back(0);
    }
    for (size_t i = 0; i < points->size(); ++i) {
      for(size_t k=0; k<dirVec.size();++k) {
        if( points->points[i].getVector3fMap().dot(dirVec[k]) < points->points[minVertVec[k]].getVector3fMap().dot(dirVec[k])) 
          minVertVec[k] = i;
        if( points->points[i].getVector3fMap().dot(dirVec[k]) > points->points[maxVertVec[k]].getVector3fMap().dot(dirVec[k])) 
          maxVertVec[k] = i;
      }
    }
    for(size_t k=0;k<dirVec.size();++k) {
      if(normals->points[minVertVec[k]].getNormalVector3fMap().normalized().dot(dirVec[k]) > angleThreshold ) voteCount++;
      if(normals->points[maxVertVec[k]].getNormalVector3fMap().normalized().dot(dirVec[k]) < -angleThreshold ) voteCount++;
    }

    if (voteCount == 0) {
      for (size_t i = 0; i < normals->size(); ++i) {
        Eigen::Vector4f normal = normals->points[i].getNormalVector4fMap();
        pcl::flipNormalTowardsViewpoint(points->points[i], 0, 0, 0, normal);
        if (normal[0] == normals->points[i].normal_x &&
            normal[1] == normals->points[i].normal_y &&
            normal[2] == normals->points[i].normal_z)
          ++voteCount;
      }
      flip_face_orientation = !(voteCount < int(points->size())/2);
      std::cout <<  voteCount*1.0/points->size() << " -> " << flip_face_orientation << "\n";
    } else {
      flip_face_orientation = !(voteCount < int(dirVec.size())/2);
      std::cout <<  voteCount*1.0/dirVec.size() << " +> " << flip_face_orientation << "\n";
    }
    if (flip_face_orientation) {
      for (size_t f = 0; f < mesh.polygons.size(); ++f) {
        std::reverse(mesh.polygons[f].vertices.begin(), mesh.polygons[f].vertices.end());
      }
      pcl::toPCLPointCloud2(*points, mesh.cloud);
      computeNormalsIfNecessary(mesh, points, normals, 18, tree, true, true);
    }
  }

  Eigen::Vector4f centroid;
  pcl::compute3DCentroid(*points, centroid);

  pcl::PolygonMesh processed_mesh;
  processed_mesh.polygons = mesh.polygons;
  pcl::PointCloud<pcl::PointNormal>::Ptr mesh_points(new pcl::PointCloud<pcl::PointNormal>);
  pcl::concatenateFields (*points, *normals, *mesh_points);
  pcl::toPCLPointCloud2(*mesh_points, processed_mesh.cloud);
  mesh_points->clear();
  printf("Draw\n");

  GLWindow *window = NULL;
  if (!extract_silhouettes or extract_silhouettes > 3) {
    window = new PCLWindow(&processed_mesh, window_width, window_width, 0, 0, "");
  } else {
    if (!use_suggestive_contours) {
      if (geometryShaderSupported())
        window = new NaiveSilhouetteWindow(&processed_mesh, window_width, window_width, 0, 0, "");
      else
        window = new SilhouetteWindow(&processed_mesh, window_width, window_width, 0, 0, "");
    } else
      window = new SuggestiveContoursWindow(&processed_mesh, window_width, window_width, 0, 0, "");
  }
  window->hide();
  window->background_color = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);
  window->foreground_color = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
  if (extract_silhouettes > 3) window->foreground_color = glm::vec4(0.8f, 0.8f, 0.8f, 1.0f);
  GLManager::addWindow(window);

  GLCamera camera = *(window->camera()); 
  glm::vec3 scene_center(centroid[0],centroid[1],centroid[2]); 
  if (verbose){
    std::cout << "cog: " << scene_center[0] << " " << scene_center[1] << " " << scene_center[2] << "\n";
    std::cout << "diameter: " << diameter << "\n";
  } 
  float distance_to_bounding = 1.0;
  camera.is_perspective = true;
  camera.fov = 60.0f*M_PI/180.0f;
  camera.zNear = 1.0f;
  camera.zFar = !use_suggestive_contours ? camera.zNear + 1.0*diameter*distance_to_bounding : camera.zNear + 2.0*diameter*distance_to_bounding;
  int width = camera.window_size[0];
  int height = camera.window_size[1];
  float* values = new float[width*height];
  
  std::vector<glm::vec3> camera_positions;
  /*if (use_random) {
    int num_samples = 6;
    if (num_maps > 0) num_samples = num_maps;
    for (int k = 0; k < num_samples; ++k) {
      camera_positions.push_back(distance_to_bounding*glm::normalize(glm::vec3(rand(), rand(), rand())));
    }
  }
  else*/ 
  // std::vector<glm::vec3> tmp_camera_positions; int indx = 4;
  // fibonacci_sphere(tmp_camera_positions, 12);  std::cout << tmp_camera_positions.size() << "\n";
  // std::cout << tmp_camera_positions[indx].x << " " << tmp_camera_positions[indx].y << " " << tmp_camera_positions[indx].z << "\n";

  if (num_maps == 1) {
    pcl::PointXYZ proj_min, proj_max; 
    pcl::getMinMax3D (*points, proj_min, proj_max); 
    proj_min.x -= centroid[0]; proj_min.y -= centroid[1]; proj_min.z -= centroid[2]; 
    proj_max.x -= centroid[0]; proj_max.y -= centroid[1]; proj_max.z -= centroid[2]; 

    // camera_positions.push_back(distance_to_bounding*glm::normalize(glm::vec3(proj_max.x, proj_max.y, -proj_max.z))); //glm::vec3(0.5, 1, 1)
    camera_positions.push_back(distance_to_bounding*glm::normalize(glm::vec3(0,0,-0.5)));
  }
  else if (num_maps > 0) fibonacci_sphere(camera_positions, num_maps, false);
  else octahedron_sphere(camera_positions, num_levels);

  if (use_random && (num_maps != 1)) {
    float pertubation_angle = (M_PI/2)/camera_positions.size();
    for (int k = 0; k < camera_positions.size(); ++k) {
      float r = glm::length(camera_positions[k]);
      float theta = acos(camera_positions[k].z/r);
      float phi = atan(camera_positions[k].y/camera_positions[k].x);
      theta += (2*pertubation_angle*((float)rand())/RAND_MAX - pertubation_angle);
      phi += (2*pertubation_angle*((float)rand())/RAND_MAX - pertubation_angle);
      camera_positions[k] = glm::vec3(r*sin(theta)*cos(phi), r*sin(theta)*sin(phi), r*cos(theta));
    }
  }

  // static int rd = 0;
  // ++rd;

  for (size_t j = 0; j < camera_positions.size(); ++j) {
    camera.position = scene_center + (0.5f*(camera.zFar-camera.zNear))*camera_positions[j];
    glm::vec3 direction = glm::normalize(scene_center - camera.position);
    float horizontal_angle = acos(direction.z);
    glm::vec3 right = glm::vec3( sin(horizontal_angle - 3.14f/2.0f), 0, cos(horizontal_angle  - 3.14f/2.0f));
    glm::vec3 up = glm::cross( right, direction );
    camera.orientation = glm::lookAt(camera.position, scene_center, up);
    // std::cout << camera.position[0] << " " << camera.position[1] << " " <<camera.position[2] << "\n";

    
    // if (rd == 5) {
    //   *(window->camera()) = camera;
    //   window->show();
    //   window->startMainLoop();
    // }

    if (!extract_silhouettes) {
      window->offscreenRender(camera, values, 
        GL_DEPTH_COMPONENT, GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT, GL_FLOAT, width, height);
    } else {
      if (use_suggestive_contours)
        ((SuggestiveContoursWindow*)window)->grabContoursImage(camera, values, width, height);
      else if (extract_silhouettes > 3) {
        window->offscreenRender(camera, values, 
          GL_RED, GL_COLOR_ATTACHMENT0, GL_RED, GL_FLOAT, width, height);
      }
      else
        window->offscreenRender(camera, values, 
          GL_RED, GL_COLOR_ATTACHMENT0, GL_RED, GL_FLOAT, width, height);
    }

    depthmaps.push_back(Eigen::Map<Eigen::MatrixXf>(values, width, height));
    depthmaps.back().transposeInPlace(); // since by default Eigen matrices are column-major
    float min_val = depthmaps.back().minCoeff();
    float max_val = depthmaps.back().maxCoeff();
    if (max_val - min_val > FLT_MIN)
      for (size_t r = 0; r < height; ++r)
        for (size_t c = 0; c < width; ++c) 
          depthmaps.back()(r, c) = ((depthmaps.back()(r, c) - min_val)*1.0/(max_val-min_val));
  } 

  delete values;
  delete window;
}

bool saveDepthMap(const std::string& map_filename, const Eigen::MatrixXf& map) {
  int width = map.cols();
  int height = map.rows();
  std::vector<std::uint16_t> img_data(width*height, 0);
  for (size_t r = 0; r < height; ++r)
    for (size_t c = 0; c < width; ++c)  
      img_data[c + r*width] = map(r, c)*65535;
  pcl::io::saveShortPNGFile(map_filename, &img_data[0], width, height, 1);
  return true;
}

bool saveDepthMaps(std::ofstream& maps_ofs, const std::string& filename, const std::string& model_id, const std::string& prefix, const std::vector<Eigen::MatrixXf>& depthmaps, bool aggregate) {
  if (!aggregate) {
    std::ofstream ofs(filename);
    for (size_t i = 0; i < depthmaps.size(); ++i) {
      std::stringstream ss; ss << model_id << "_" << i;
      std::string map_id =  ss.str();
      std::string output_filename = 
        generate_output_filename("", ".png", map_id, prefix);
      if (saveDepthMap(output_filename, depthmaps[i])) {
        ofs << map_id << " " << output_filename << "\n";
        maps_ofs << map_id << " " << output_filename << "\n";
      }
    }
    ofs.close();
  } else {
    int cols = ceil(sqrt(depthmaps.size()));
    int rows = ceil(depthmaps.size()*1.0f/cols);
    if (cols < rows) { int tmp = rows; rows = cols; cols = tmp;}
    int map_rows = depthmaps.front().rows();
    int map_cols = depthmaps.front().cols();
    int offset = 10;
    Eigen::MatrixXf big_map = Eigen::MatrixXf::Constant(rows*(map_rows+offset), cols*(map_cols+offset), 1.0);
    for (int d = 0; d < (int) depthmaps.size(); ++d) {
      int row = d/cols, col = d%cols;
      for (size_t i = 0; i < map_cols; ++i) {
        for (size_t j = 0; j < map_rows; ++j) 
          big_map(j+row*(map_rows+offset), i + col*(map_cols+offset)) = depthmaps[d](j, i);
      }
    }
    return saveDepthMap(filename, big_map);
  }
  return true;
}

int main (int argc, char** argv) {
  // Parse cmd flags
  std::map<std::string, std::string> flags;
  parseCmdArgs(argc, argv, &flags);
  bool arg_error = false;

  std::string model_filename, model_filelist;
  std::string output_filelist, maps_filelist, output_prefix;
  size_t num_maps, num_levels, window_width;
  bool verbose, aggregate, use_random;
  int extract_silhouettes;
  if (!(getFlag(flags, "model", "",  &model_filename) ^
        getFlag(flags, "model_filelist", "",  &model_filelist))) {
    printf("Error: specify --model xor --model_filelist\n");
    arg_error = true;
  }
  if (model_filelist.size() > 0 &&
      !getFlag(flags, "output_filelist", "",  &output_filelist)) {
    printf("Error: specify --output_filelist\n");
    arg_error = true;
  }
  if (!getFlag(flags, "output_prefix", "",  &output_prefix)) {
    printf("Error: specify --output_prefix\n");
    arg_error = true;
  }
  
  getFlag(flags, "num_maps", (size_t) 0,  &num_maps);
  getFlag(flags, "window_width", (size_t) 256,  &window_width);
  getFlag(flags, "num_levels", (size_t) 1,  &num_levels);
  getFlag(flags, "use_random", true,  &use_random);
  getFlag(flags, "verbose", false,  &verbose);
  getFlag(flags, "aggregate", true,  &aggregate);
  getFlag(flags, "extract_silhouettes", 2,  &extract_silhouettes);

  if (!aggregate && model_filelist.size() > 0 && !getFlag(flags, "maps_filelist", "",  &maps_filelist)) {
    printf("Error: specify --maps_filelist\n");
    arg_error = true;
  }

  std::string output_suffix = aggregate ? (num_maps==1?".png":"_maps.png") : "_maps.txt";

  if (arg_error) return 1;

  GLManager& manager = GLManager::getInstance();
  std::ofstream maps_ofs;

  if (!aggregate) maps_ofs.open(maps_filelist.c_str());

  if (model_filename.size() > 0) {
    pcl::PolygonMesh mesh;
    if (!pcl::io::loadPolygonFile(model_filename, mesh)) {
      printf("Error: could not load model from %s.\n", model_filename.c_str());
      return 1;
    }
    std::vector<Eigen::MatrixXf> depthmaps;
    generate_depthmaps(mesh, depthmaps, extract_silhouettes, window_width, num_levels, num_maps, use_random, verbose);
    std::string dir, model_id;
    get_dir_and_basename(model_filename, dir, model_id);
    std::string output_filename = 
      generate_output_filename(model_filename, output_suffix, "", output_prefix);
    saveDepthMaps(maps_ofs, output_filename, model_id, output_prefix, depthmaps, aggregate);
  }

  int count = 0;
  if  (model_filelist.size() > 0) {
    std::ifstream model_filelist_in (model_filelist.c_str());
    std::ofstream output_filelist_out (output_filelist.c_str());

    while (!model_filelist_in.eof()) {
      std::string model_id, filename;
      model_filelist_in >> model_id >> filename;
      if (model_id.size() == 0 && filename.size() == 0) continue;

      pcl::PolygonMesh mesh;
      if (!pcl::io::loadPolygonFile(filename, mesh)) {
        printf("Error: could not load model from %s.\n", filename.c_str());
        continue;
      }
      if (verbose) printf("Processing: %s %s\n", model_id.c_str(), filename.c_str());
      std::vector<Eigen::MatrixXf> depthmaps;
      generate_depthmaps(mesh, depthmaps, extract_silhouettes, window_width, num_levels, num_maps, use_random, verbose);
      std::string output_filename = 
        generate_output_filename(filename, output_suffix, model_id, output_prefix);
      if (saveDepthMaps(maps_ofs, output_filename, model_id, output_prefix, depthmaps, aggregate)) {
        output_filelist_out << model_id << " " << output_filename << "\n";
      }
      ++count;
      // if (count == 5) break;
    }

    model_filelist_in.close();
    output_filelist_out.close();
  }

  if (!aggregate) maps_ofs.close();
}